// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"crypto/rand"
	mrand "math/rand"

	"golang.org/x/crypto/ssh"
)

func main() {
	// ruleid: go_crypto_rule-insecure-ignore-host-key
	_ = ssh.InsecureIgnoreHostKey()
}

func bad2() {
	good, _ := rand.Read(nil)
	println(good)
	// ruleid: go_crypto_rule-weakrandsource
	bad := mrand.Int31()
	println(bad)
}
